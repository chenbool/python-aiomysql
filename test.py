import asyncio
import aiomysql




async def test_example():
    global __pool
    __pool = await aiomysql.create_pool(
        host='127.0.0.1',
        port=3306,
        user='root',
        password='root',
        db='mysql',
        loop=loop
    )

    async with __pool.acquire() as conn:
        async with conn.cursor(aiomysql.DictCursor) as cur:
            await cur.execute("SELECT Host,User FROM user")
            # print(cur.description)
            r = await cur.fetchall()
            print(r)
            await cur.close()
        conn.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(test_example())